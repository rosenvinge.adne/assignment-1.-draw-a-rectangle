import java.util.Scanner;
import java.util.*;

public class Rectangle{
    
    static int width, height;
    static String character;

    // calculates the large and small squares and print them using 'print_grid'
    public static void draw_rectangle(){

        boolean[][] grid = new boolean[height][width];
        
        // large square
        for (int w = 0; w < width; w++){ 
            for (int h = 0; h < height; h++){

                if(
                    h == 0 ||
                    h == height-1 ||
                    w == 0 ||
                    w == width-1
                ){
                    grid[h][w] = true;
                }else{
                    grid[h][w] = false;
                }
            }
        }
        
        // small square
        if((height >= 7) && (width >= 7)){
            for (int w = 2; w < width - 2; w++){
                for (int h = 2; h < height - 2; h++){
                    if(
                        h == 2 ||
                        h == height-3 ||
                        w == 2 ||
                        w == width-3
                    ){
                        grid[h][w] = true;
                    }
                }
            }
        }

        print_grid(grid);
    }

    // prints the grid
    public static void print_grid(boolean[][] grid){

        for(boolean[] list : grid){
            for (boolean b : list) {
                if (b){
                    System.out.print(character);
                }else{
                    // Allows the user to type words, not just characters
                    for(int i = 0; i < character.length(); i++){ 
                        System.out.print(" ");
                    }

                }
            }
            System.out.println();
        }

    }

    //  All userinput regarding the square dimentions and character.
    public static void fetch_rect_dim(){

        Scanner scanner = new Scanner(System.in);
        
        boolean illegaleInput = true;
        while (illegaleInput){

            System.out.println("\n\nWrite the square dimentions and character \nExample '8 x 5 square written with #':\n8 x 5 #");

            String input = scanner.nextLine();
            String[] split = input.split(" ");

            try{
                width = Integer.parseInt(split[0]);
                height = Integer.parseInt(split[2]);
                character = split[3];
                
                illegaleInput = false;
            }catch (Exception e){
                System.out.println("Error: input is not valid, try again");
            }
        }
    }
    
    
    
    public static void main(String [] arg){

        Scanner scanner = new Scanner(System.in);

        boolean fortsette = true;  // cant spell the english word
        
        while (fortsette){ // enables multiple squares

            fetch_rect_dim();

            draw_rectangle();

            System.out.print("press 's' to make a new square: \n");

            String con = scanner.nextLine();

            if (!con.equals("s")){
                System.out.println("All done ...");
                fortsette = false;
            }   
        }
    }

}
