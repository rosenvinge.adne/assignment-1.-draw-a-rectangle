# Task 1 - Draw a rectangle

Note: I forgot to add a README, this was added after the task deadline.

## About

This program asks the user for the dimentions for a square and prints it to the terminal in the character the user wants. I also draw a second square inside if the dimentions allow it. 

## Implementation

I use a 2d array to "locate" what is to be a blank space and what is to be a square. This was not in the original, but was added to easy implement the smaller square. I also added a seperate method to print the square, this was to cepp the code as clean as polible.

## Extra

I also made it so the user can build a square out of multiple characters. To make this work i also made it so the blank spaces in the square is as many spaces as the character is long.
